/******************************************************************************
Run using GCC C Compiler
*******************************************************************************/
/*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/* Compute Slope */
// Input: y = coordinate values and
//      : n = number of inputs
// Output: slope = slope of the online
//       : b = offset
//Calculations : 
//              slope = ((xmean*ymean) - xymean)/(xmeansqr - xsqrmean)
//              b = ymean - slope*xmean;
float * slopenoffset(int *x, int *y, int n){
    float *output;
    int i;
    float slope, offset, xmean=0.0, ymean=0.0, xymean=0.0, xmeansqr=0.0, xsqrmean=0.0;
    output = (float *)malloc(2*sizeof(float));
    for(i=0; i<n; i++){
        xmean = *(x+i) + xmean;
        ymean = ymean + *(y+i);
        xymean = xymean + ((*(x+i))*(*(y+i)));
        xsqrmean = xsqrmean + ((*(x+i))*(*(x+i)));
    }
    xmean = xmean/n;
    ymean = ymean/n;
    xymean = xymean/n;
    xmeansqr = xmean*xmean;
    xsqrmean = xsqrmean/n;
    slope = ((xmean*ymean) - xymean)/(xmeansqr - xsqrmean);
    offset = ymean - slope*xmean;
    *(output+0) = slope;
    *(output+1) = offset;
    return output;
}

int main(){
    int *y, *x, n, i, key, ind, *newy;
    float *mb, m, b;
    printf("Enter number of samples:\n");
    scanf("%d", &n);
    y = (int *) malloc(n*sizeof(int));
    x = (int *) malloc(n*sizeof(int));
    printf("Enter x features:\n");
    //Get the y coordinate values
    for (i=0;i<n;i++){
        scanf("%d", x+i);
    }
    printf("Enter y features:\n");
    //Get the y coordinate values
    for (i=0;i<n;i++){
        scanf("%d", y+i);
    }
    mb = slopenoffset(x, y, n);
    m = *(mb + 0);
    b = *(mb + 1);
    printf("Computed Slope: %f\n",m);
    printf("Computed Offset: %f\n",b);
    newy = (int *) malloc(n*sizeof(int));
    printf("Line is given as:\n");
    printf("x | new y\n");
    for(i=0;i<n; i++){
        *(newy+i) = round(m*(*(x+i)) + b);
        printf("%d | %d\n", *(x+i), *(newy+i));
    }
    return 0;
}